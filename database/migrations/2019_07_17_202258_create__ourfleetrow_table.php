<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOurfleetrowTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Ourfleetrow', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ourfleetcaption');
            $table->longText('ourfleetmore');
            $table->string('ourfleetimage');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Ourfleetrow');
    }
}
