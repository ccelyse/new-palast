<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHireaguideTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Hireaguide', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('attractionid');
            $table->foreign('attractionid')->references('id')->on('Attractions');
            $table->string('name');
            $table->string('email');
            $table->string('contactnumber');
            $table->string('country');
            $table->string('totaldults');
            $table->string('totalchildren');
            $table->longText('message');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Hireaguide');
    }
}
