<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FlightBookingRow extends Model
{
    protected $table = "flightbookingrow";
    protected $fillable = ['id','flightbookingcaption','flightbookingimage','flightbookingmore'];
}
