<?php

namespace App\Http\Controllers;

use App\AboutUsRow;
use App\ActivityDetails;
use App\ActivitySummary;
use App\Attractions;
use App\Countries;
use App\EnquireNow;
use App\FleetGallery;
use App\FlightBookingRow;
use App\HomeSlider;
use App\JoinMember;
use App\Mail\ClientInquire;
use App\Mail\OrderConfirmation;
use App\OurFleetRow;
use App\PaymentTransactions;
use App\Post;
use App\SafariExperienceRow;
use App\SetBookingPrice;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
require ('bk-payment/function.php');
class FrontendController extends Controller
{


    public function Home(){
        $listslider = HomeSlider::orderBy('id','desc')->limit('5')->get();
        $listattractions = ActivitySummary::all();

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://palasttours.rw/public/blog/wp-json/wp/v2/posts?categories=65",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $response_data =json_decode($response);

//        $listattractions = Attractions::all();
        return view('welcome')->with(['listattractions' => $listattractions,'listslider'=>$listslider,'response_data'=>$response_data]);
    }

    public function FlightBooking(){
        $listflight = FlightBookingRow::all();
        return view('FlightBooking')->with(['listflight'=>$listflight]);
    }
    public function SafariExperiences(){
//        $listattractions = Attractions::all();
        $listdestination = ActivitySummary::all();
        $listsafari = SafariExperienceRow::all();
        return view('SafariExperiences')->with(['listsafari'=>$listsafari,'listdestination'=>$listdestination]);
//        return view('SafariExperiences');
    }
    public function ActivityMore(Request $request){
        $all = $request->all();
        $destinationid = $request['id'];
        $listmore = ActivitySummary::where('id',$destinationid)->get();
//        dd($listmore);
        $getdetails = ActivityDetails::where('activitysummary_id',$destinationid)->get();
//        $listcountries = Countries::all();
        return view('ActivityMore')->with(['getdetails'=>$getdetails,'listmore'=>$listmore]);

    }
    public function PalastAssist(){
        return view('PalastAssist');
    }
    public function OurFleet(){
        $listfleet = OurFleetRow::all();
        $gallery = FleetGallery::all();
        return view('OurFleet')->with(['listfleet'=>$listfleet,'gallery'=>$gallery]);
    }
    public function AboutUs(){
        $listabout = AboutUsRow::all();
        return view('Aboutus')->with(['listabout'=>$listabout]);
    }
    public function Destinations(){
        $listattractions = Attractions::all();
        return view('Destinations')->with(['listattractions' => $listattractions]);
    }
    // public function Destinations(){
    //     return view('Destinations');
    // }
    public function ContactUs(){
        return view('ContactUs');
    }
    public function Login(){
        return view('backend.Login');
    }
    public function Webmail(){
        return redirect('https://mail.rha.rw/');
    }
    public function DomainTest(){
        return view('backend.DomainTest');
    }
    public function Inquire(Request $request){
        $id = $request['id'];
        $list_countries = Countries::all();
        return view('Inquire')->with(['id'=>$id,'list_countries'=>$list_countries]);
    }
    public function InquireNow(Request $request){
        $all = $request->all();
        $add = new EnquireNow();
        $currentMonth = Carbon::now()->format('m');
        $code = mt_rand(10, 99). $currentMonth;

        $add->enquire_title = $request['enquire_title'];
        $add->destination_id = $request['destination_id'];
        $add->enquire_first_name = $request['enquire_first_name'];
        $add->enquire_last_time = $request['enquire_last_time'];
        $add->enquire_telephone = $request['enquire_telephone'];
        $add->enquire_email = $request['enquire_email'];
        $add->enquire_country = $request['enquire_country'];
        $add->enquire_subject = $request['enquire_subject'];
        $add->enquire_message = $request['enquire_message'];
        $add->enquire_code = $code;
        $add->save();

        $Fname = $request['enquire_first_name'];
        $Lname = $request['enquire_last_time'];
        $email = $request['enquire_email'];
        $message = $request['enquire_message'];
        $subjectmail = $request['enquire_subject'];

        $content = [
            'title'=> $Fname,
            'namemail'=>'Palast Tours',
            'name' => $Fname,
            'email' => $email,
            'body' => $message,
            'Fname' => $Fname,
            'Lname' => $Lname,
            'enquire_code' => $code,
            'enquire_telephone' => $request['enquire_telephone'],

//            'button' => 'Click Here'
        ];

        $receiverAddress = $email;

        Mail::to($receiverAddress)->send(new ClientInquire($content));

        return back()->with('success','you successfully sent an inquire, we shall get back to in less than 24 hours');
    }

    public function PayNow(Request $request){
        $id = $request['id'];
//        dd($id);
//        $id = '1';
        $getinfo = SetBookingPrice::where('booking_id',$id)->join('enquire_now', 'enquire_now.id', '=', 'set_booking_prices.booking_id')
                ->select('enquire_now.destination_id','enquire_now.enquire_title','enquire_now.enquire_first_name',
                    'enquire_now.enquire_title','enquire_now.enquire_title','enquire_now.enquire_title',
                    'enquire_now.enquire_last_time','enquire_now.enquire_telephone','enquire_now.enquire_email',
                    'enquire_now.enquire_country','set_booking_prices.*')
                ->get();
        return view('PayNow')->with(['id'=>$id,'getinfo'=>$getinfo,]);
    }
    public function MakePayment(Request $request){
        $all = $request->all();
        $booking_price = $request['booking_price'];
        $newamount = ltrim($booking_price, '$');
        $amount = $newamount;
        $currency = 'USD';
        //'RWF';
        $currentMonth = Carbon::now()->format('m');
        $code = mt_rand(10, 99999). $currentMonth;
        $orderInfo = $code;

        $getenquriecode = EnquireNow::where('id',$request['booking_id'])->value('enquire_code');
        $transaction = new PaymentTransactions();
        $transaction->vpc_Amount = $amount;
        $transaction->enquire_code = $getenquriecode;
        $transaction->vpc_OrderInfo = $orderInfo;
        $transaction->save();

        /**
         * Our merchant account configurations.. How to acquire those?
         *
         * This data will be encrypted and saved securely in our database..
         */

        // active
        $accountData = array(
            'merchant_id' => config('app.MERCHANT'),
            'access_code' => config('app.ACCESS_CODE'),
            'secret'      => config('app.SECRET'),
        );

// multi currency

        if($currency=="RWF"){
            // $_PDT['vpc_Currency']=646;
            $mult = 1;
        }
        elseif($currency=="USD"){
            // $_PDT['vpc_Currency']=840;
            $mult = 100;
        }

        /**
         * Query data..
         */
        $queryData = array(
            'vpc_AccessCode' => $accountData['access_code'],
            'vpc_Merchant' => $accountData['merchant_id'],

            'vpc_Amount' => ($amount * $mult), // Multiplying by 100 to convert to the smallest unit
            'vpc_OrderInfo' => $orderInfo,

            'vpc_MerchTxnRef' => generateMerchTxnRef(), // See functions.php file

            'vpc_Command' => 'pay',
            'vpc_Currency' => $currency,
            'vpc_Locale' => 'en',
            'vpc_Version' => 1,

//            'vpc_ReturnURL' => 'https://akokanya.com/payment-callback',
//            'vpc_ReturnURL' => 'http://127.0.0.1:9038/payment-callback',
            'vpc_ReturnURL' => 'https://payment.palasttours.rw/public/payment-callback',
            'vpc_SecureHashType' => 'SHA256'
        );


// Add secure secret after hashing
        $queryData['vpc_SecureHash'] = generateSecureHash($accountData['secret'], $queryData); // See functions.php file
        $migsUrl = 'https://migs.mastercard.com.au/vpcpay?'.http_build_query($queryData);
        $migsUrlProduction = 'https://migs-mtf.mastercard.com.au/vpcpay?'.http_build_query($queryData);

        return \Illuminate\Support\Facades\Redirect::to($migsUrlProduction);
        header("Location: " . $migsUrlProduction);
    }
    public function PaymentCallback(Request $request){
        $datenow = Carbon::now();
        $all = $request->all();
        $getinquirecode = PaymentTransactions::where('vpc_OrderInfo',$request['vpc_OrderInfo'])->value('enquire_code');
        $getemail = EnquireNow::where('enquire_code',$getinquirecode)->get();

        foreach ($getemail as $data){
            $email = $data->enquire_email;
            $name = $data->enquire_first_name;
            $PackageCode = $data->destination_id;
            $BookingNumber = $data->enquire_code;
        }
//        dd($getemail);

        $newamount = $request['vpc_Amount'] / 100;
        $PaymentMethod = $request['vpc_Card'];
        $CardNumber = $request['vpc_CardNum'];
        $AuthorizationNumber = $request['vpc_AuthorizeId'];
        $PaymentStatus = $request['vpc_Message'];
        $PaymentDate = $datenow;
        $PackageCode_ = $PackageCode;
        $BookingNumber_ = $BookingNumber;

        $Transaction = PaymentTransactions::where('vpc_OrderInfo',$request['vpc_OrderInfo'])
            ->update([
                'vpc_BatchNo'=> $request['vpc_BatchNo'],
                'vpc_Card'=> $request['vpc_Card'],
                'vpc_CardNum'=> $request['vpc_CardNum'],
                'vpc_Currency'=> $request['vpc_Currency'],
                'vpc_MerchTxnRef'=> $request['vpc_MerchTxnRef'],
                'vpc_Merchant'=> $request['vpc_Merchant'],
                'vpc_Message'=> $request['vpc_Message'],
                'vpc_OrderInfo'=> $request['vpc_OrderInfo'],
                'vpc_ReceiptNo'=> $request['vpc_ReceiptNo'],
                'vpc_SecureHash'=> $request['vpc_SecureHash'],
                'vpc_SecureHashType'=> $request['vpc_SecureHashType'],
                'vpc_TransactionNo'=> $request['vpc_TransactionNo'],
                'vpc_VerStatus'=> $request['vpc_VerStatus'],
                'vpc_VerToken'=> $request['vpc_VerToken'],
            ]);

        $message = "We have successfully received your payment of the amount of $ $newamount";

        if($request['vpc_Message'] == 'Approved'){
            $message = "This is to certify that you have successfully paid a total amount of USD ($) $newamount for booking your preferred  tourist package.";
            $subjectmail = "Payment successfully processed";

            $data = array(
                'namemail'=>'Palast Tours',
                'messagemail'=>$message,
            );

//            Mail::send('mail',
//                array(
//                    'name' => $name,
//                    'email' => $email,
//                    'messagemail' => $message,
//                    'PackageCode_' => $PackageCode_,
//                    'BookingNumber_' => $BookingNumber_,
//                    'newamount' => $newamount,
//                    'PaymentMethod' => $PaymentMethod,
//                    'CardNumber' => $CardNumber,
//                    'AuthorizationNumber' => $AuthorizationNumber,
//                    'PaymentStatus' => $PaymentStatus,
//                    'PaymentDate' => $PaymentDate,
//                ), function($message) use ($email, $name,$subjectmail,$PackageCode_,$BookingNumber_,$newamount,$PaymentMethod,$CardNumber,$AuthorizationNumber,$PaymentStatus,$PaymentDate)
//                {
//                    $message->from('kalisa913@gmail.com', 'Palast Tours');
//                    $message->to($email, 'Palast Tours')->subject($subjectmail);
//                });

            $content = [
                'title'=> $name,
                'email' => $email,
                'PackageCode_' => $PackageCode_,
                'BookingNumber_' => $BookingNumber_,
                'newamount' => $newamount,
                'PaymentMethod' => $PaymentMethod,
                'CardNumber' => $CardNumber,
                'AuthorizationNumber' => $AuthorizationNumber,
                'PaymentStatus' => $PaymentStatus,
                'PaymentDate' => $PaymentDate,
                'body'=> $message,
//            'button' => 'Click Here'
            ];
            $receiverAddress = $email;

            Mail::to($receiverAddress)->send(new OrderConfirmation($content));

            return redirect()->route('PaymentRe')->with('success',$message);
        }else{
            return redirect()->route('PaymentRe')->with('success','Your Transaction has failed , kindly try again');
        }
    }
    public function PaymentRe(){
        return view('PaymentRe');
    }
    public function Receipt(){
        $code = '3182210';

        $getenquirecode = PaymentTransactions::where('vpc_OrderInfo',$code)->value('enquire_code');
        $vpc_Amount = PaymentTransactions::where('vpc_OrderInfo',$code)->value('vpc_Amount');
        $getinfo = PaymentTransactions::where('vpc_OrderInfo',$code)->get();
        $getnames = EnquireNow::where('enquire_code',$getenquirecode)->get();
        $getdestinationid = EnquireNow::where('enquire_code',$getenquirecode)->value('destination_id');
        $getdestinationinfo = Attractions::where('id',$getdestinationid)->value('attraction_name');

        return view('Receipt')->with(['getinfo'=>$getinfo,'getnames'=>$getnames,'vpc_Amount'=>$vpc_Amount,'getdestinationinfo'=>$getdestinationinfo]);
    }
    public function AttractionAPI(){
        $listattractions = Attractions::select('id','attraction_province','attraction_name','attraction_image')->get();
        return response()->json($listattractions);
    }
    public function MoreAttractionAPI(Request $request){
        $id = $request['id'];
        $listattractions = Attractions::where('id',$id)->get();
        return response()->json($listattractions);
    }

    public function AttractionsMore(Request $request){
        $all = $request->all();
        $attraction_id = $request['id'];
        $listmore = Attractions::where('id',$attraction_id)->get();
        $listcountries = Countries::all();
        return view('AttractionsMore')->with(['listmore'=>$listmore,'listcountries'=>$listcountries]);

    }

    public function SendingEmail(Request $request) {
        $all = $request->all();
//        dd($all);

        $name = $request['names'];

        $email = $request['email'];
        $message = $request['message'];
        $subjectmail = $request['subject'];

        $data = array(
            'namemail'=>'Palast Tours',
            'messagemail'=>$message,
        );


        Mail::send('mail',
            array(
                'namemail'=>'Palast Tours',
                'name' => $name,
                'email' => $email,
                'messagemail' => $message
            ), function($message) use ($email, $name,$subjectmail)
            {
                $message->from($email, $name);
                $message->to('info@palasttours.rw', 'Palast Tours')->subject($subjectmail);
            });

        return view('ThankYou');
    }

    public function ThankYou(){
        return view('ThankYou');
    }
    public function blog(){
        return \File::get(pulbic_path() . '/blog/index.php');
    }
    public function GetBlog(){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://palasttours.rw/public/blog/wp-json/wp/v2/posts?categories=65",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $response_data =json_decode($response);
        dd($response_data);
//
      
///


//        $curl = curl_init();
//
//        curl_setopt_array($curl, array(
//            CURLOPT_URL => "https://palasttours.rw/public/blog/wp-json/wp/v2/media/1040",
//            CURLOPT_RETURNTRANSFER => true,
//            CURLOPT_ENCODING => "",
//            CURLOPT_MAXREDIRS => 10,
//            CURLOPT_TIMEOUT => 0,
//            CURLOPT_FOLLOWLOCATION => true,
//            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//            CURLOPT_CUSTOMREQUEST => "GET",
//        ));
//
//        $response = curl_exec($curl);
//
//        curl_close($curl);
//
//        $response_data =json_decode($response);
//        dd($response_data->source_url);
    }


}
