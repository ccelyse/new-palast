<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FleetGallery extends Model
{
    protected $table = "FleetGallery";
    protected $fillable = ['id','fleetgallery'];
}
