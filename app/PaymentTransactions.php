<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentTransactions extends Model
{
    protected $table = "PaymentTransactions";
    protected $fillable = ['id','vpc_Amount','enquire_code','vpc_BatchNo','vpc_Card','vpc_CardNum','vpc_Currency','vpc_MerchTxnRef',
        'vpc_Merchant','vpc_Message','vpc_OrderInfo','vpc_ReceiptNo','vpc_SecureHash','vpc_SecureHashType','vpc_TransactionNo',
        'vpc_VerStatus','vpc_VerToken'];
}
