<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityDetails extends Model
{
    protected $table = "activitydetails";
    protected $fillable = ['id','activitysummary_id','activityday','activitylocation','activitydetails'];
}
