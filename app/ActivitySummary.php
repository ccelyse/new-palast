<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivitySummary extends Model
{
     protected $table = "activitysummary";
    protected $fillable = ['id','activitytitle','activitysummary','activityimage'];
}
