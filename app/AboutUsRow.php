<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AboutUsRow extends Model
{
    protected $table = "aboutusrow";
    protected $fillable = ['id','aboutuscaption','aboutusimage','aboutusmore'];
}
