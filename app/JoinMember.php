<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JoinMember extends Model
{
    protected $table = "JoinMember";
    protected $fillable = ['id','typeofmembership','chamber','companyname','companycode','hotelcategory','numberofrooms','gender','phonenumber','pobx','email','website','businessaddress','buildingname','businessarea','province','district','sector','cell','companytype','ownership','businessactivity','export','numberofemployees','numberofemployeesapart','documentname','identification','approval'];
}
