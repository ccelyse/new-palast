<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SafariExperienceRow extends Model
{
    protected $table = "safariexperiencerow";
    protected $fillable = ['id','safariexperiencecaption','safariexperienceimage','safariexperiencemore'];
}
