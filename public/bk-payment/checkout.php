<?php
exit("here");
/**
 * A sample code to illustrate how a payment request will be made from 
 * "qbrando" in the checkout process. 
 *
 * This sample is only for illustration and hasn't been tested yet.
 * 
 * @author Kareem Mohamed <kareem3d.a@gmail.com> 
 */

require __DIR__ . '/function.php';

/**
 * The following are the variables passed from the checkout process
 */

$amount = 1;
$currency = 'USD';
$orderInfo = 'ORDER34525';

/**
 * Our merchant account configurations.. How to acquire those?
 *
 * This data will be encrypted and saved securely in our database..
 */

// active
// $accountData = array(
//     'merchant_id' => 'BOK000007',
//     'access_code' => '0F36DDE7',
//     'secret'      => 'EEBE828B3C690B8F0DC2552341844E34'
// );

//test

//$accountData = array(
//    'merchant_id' => 'TESTBOK000003',
//    'access_code' => 'D4342031',
//    'secret'      => '85A3C14B3871EB7802D07072B25O959C'
//);

//$accountData = array(
//    'merchant_id' => 'BOK000007',
//    'access_code' => '0F36DDE7',
//    'secret'      => 'EEBE828B3C690B8F0DC2552341844E34'
//);

// multi currency

if($currency=="RWF"){
    // $_PDT['vpc_Currency']=646;
    $mult = 1;
}elseif($currency=="USD"){
    // $_PDT['vpc_Currency']=840;
    $mult = 100;
}
/**
 * Query data..
 */
$queryData = array(
    'vpc_AccessCode' => $accountData['access_code'],
    'vpc_Merchant' => $accountData['merchant_id'],

    'vpc_Amount' => ($amount * $mult), // Multiplying by 100 to convert to the smallest unit
    'vpc_OrderInfo' => $orderInfo,

    'vpc_MerchTxnRef' => generateMerchTxnRef(), // See functions.php file

    'vpc_Command' => 'pay',
    'vpc_Currency' => $currency,
    'vpc_Locale' => 'en',
    'vpc_Version' => 1,
    //'vpc_ReturnURL' => 'http://127.0.0.1/bk-payment/return_url.php',

    'vpc_ReturnURL' => 'http://localhost:8000/payment-callback/',

    'vpc_SecureHashType' => 'SHA256'
);

// Add secure secret after hashing
$queryData['vpc_SecureHash'] = generateSecureHash($accountData['secret'], $queryData); // See functions.php file

// 
$migsUrl = 'https://migs.mastercard.com.au/vpcpay?'.http_build_query($queryData);

// Redirect to the bank website to continue the 
header("Location: " . $migsUrl);