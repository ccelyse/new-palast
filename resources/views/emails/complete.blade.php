@component('mail::message')
<strong>Hello,{{ $content['title'] }}</strong>

{{ $content['body'] }}
{{ $content['message2'] }}

@component('mail::button', ['url' => $content['url_1']])
    Complete Your Inquire
@endcomponent

@component('mail::button', ['url' => $content['url_download']])
    Please download the attached invoice.
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
