@component('mail::message')
<strong>Hello,{{ $content['title'] }}</strong>

{{ $content['body'] }}

<strong>Order Details</strong><br>
@component('mail::table')
    | Description       |          Details |
    | ------------- |:-------------:|
    | Package Code      | {{ $content['PackageCode_'] }}      |
    | Booking Number      | {{ $content['BookingNumber_'] }} |
    | Total Paid Amount ($)      | {{ $content['newamount'] }}     |
    | Payment Method      | {{ $content['PaymentMethod'] }} |
    | Card Number      | {{ $content['CardNumber'] }}      |
    | Authorization Number      | {{ $content['AuthorizationNumber'] }} |
    | Payment Status      | {{ $content['PaymentStatus'] }}      |
    | Payment Date      | {{ $content['PaymentDate'] }} |

@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
