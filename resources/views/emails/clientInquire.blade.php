@component('mail::message')
<strong>Hello,{{ $content['title'] }}</strong>

    {{ $content['body'] }}

<strong>Client Information,</strong><br>

@component('mail::table')
    | Description       | Details         |
    | ------------- |:-------------:|
    | First Name      |    {{ $content['Fname'] }} |
    | Last Name      |     {{ $content['Lname'] }} |
    | Phone Number       |    {{ $content['enquire_telephone'] }} |
    | Email      |    {{ $content['email'] }} |
    | Inquire Code      |    {{ $content['enquire_code'] }} |

@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
