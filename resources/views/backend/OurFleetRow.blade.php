@extends('backend.layout.master')

@section('title', 'Palast')

@section('content')
    <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <style>
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #ff9027 !important;
            border-color: #ff9027 !important;
        }
        .btn-primary:hover{
            background-color: #ff9027 !important;
            border-color:#ff9027 !important;
        }
        .remove-field,.add-field{
            width: 40px;
            height: 40px;
            padding: 10px;
            margin: 25px;
        }
    </style>
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script type="text/javascript">
        $(document).ready(function()
        {
            $('.multi-field-wrapper').each(function() {
                var $wrapper = $('.multi-fields', this);
                $(".add-field", $(this)).click(function(e) {
                    $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
                });
                $('.multi-field .remove-field', $wrapper).click(function() {
                    if ($('.multi-field', $wrapper).length > 1)
                        $(this).parent('.multi-field').remove();
                });
            });
        });
    </script>
    {{--<script--}}
    {{--src="https://code.jquery.com/jquery-3.3.1.min.js"--}}
    {{--integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="--}}
    {{--crossorigin="anonymous"></script>--}}

    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body">
                <div class="content-body">
                    <!-- Basic Summernote start -->
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    <h4 class="card-title" id="basic-layout-form">OUR FLEET Info</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('AddOurFleetRow') }}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="projectinput1">Our Fleet Caption</label>
                                                        <input type="text" id="projectinput1" class="form-control" value="{{ old('ourfleetcaption') }}"
                                                               name="ourfleetcaption" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="projectinput1">Our Fleet  Image</label>
                                                        <input type="file" id="projectinput1" class="form-control"
                                                               name="ourfleetimage" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <section id="basic" style="width: 100%;">
                                                    <div class="card">
                                                        <div class="card-content collapse show">
                                                            <div class="card-body">
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-lg-12">
                                                                                <textarea class="summernote"  name="ourfleetmore" id="ourfleetmore"  required>{{ old('ourfleetmore') }}</textarea>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </section>
                                            </div>
                                            <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Save</button>
                                        </form>

                                        <h4 class="card-title" id="basic-layout-form" style="padding-top: 15px">OUR FLEET Gallery</h4>

                                        <div class="multi-field-wrapper">
                                            <form class="form-horizontal form-simple" method="POST" action="{{ url('AddFleetGallery') }}" enctype="multipart/form-data">
                                                {{ csrf_field() }}
                                            <div class="multi-fields">
                                                <div class="row  multi-field">

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Gallery</label>
                                                            <input type="file" id="projectinput1" class="form-control"
                                                                   name="fleetgallery">
                                                        </div>
                                                    </div>

                                                    {{--<button type="button" class="remove-field btn btn-dark" style="line-height: inherit !important;"><i class="fas fa-times-circle"></i></button>--}}
                                                    {{--<button type="button" class="add-field btn btn-dark" style="line-height: inherit !important;"><i class="fas fa-plus-circle"></i></button>--}}

                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Save</button>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <section id="summernote-edit-save" hidden="">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Summernote Click to edit</h4>
                                        <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body">
                                            <form class="form-horizontal" action="#">
                                                <div class="form-group">
                                                    <button id="edit" class="btn btn-primary" type="button"><i class="la la-pencil"></i> Edit</button>
                                                    <button id="save" class="btn btn-success" type="button"><i class="la la-save"></i> Save</button>
                                                </div>
                                                <div class="form-group">
                                                    <div class="summernote-edit">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section id="setting">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        {{--<h4 class="card-title">Destination list</h4>--}}
                                        @if (session('success'))
                                            <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                                {{ session('success') }}
                                            </div>
                                        @endif
                                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <table class="table table-striped table-bordered dataex-html5-export table-responsive">
                                                <thead>
                                                <tr>
                                                    <th>Our Fleet  Caption</th>
                                                    <th>Our Fleet Image</th>
                                                    <th>Our Fleet More</th>
                                                    <th>Edit</th>
                                                    <th>Delete</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($listsafari as $data)
                                                    <tr>
                                                        <td>{{$data->ourfleetcaption}}</td>
                                                        <td><img src="OurFleet/{{$data->ourfleetimage}}" style="width: 100%;padding-bottom: 10px"></td>
                                                        <td>
                                                            <button type="button" class="btn btn-icon btn-outline-primary"
                                                                    data-toggle="modal"
                                                                    data-target="#summary{{$data->id}}">
                                                                Our Fleet More
                                                            </button>
                                                            <!-- Modal -->
                                                            <div class="modal fade text-left" id="summary{{$data->id}}" tabindex="-1"
                                                                 role="dialog" aria-labelledby="myModalLabel1"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title" id="myModalLabel1"> Our Fleet  More</h4>
                                                                            <button type="button" class="close" data-dismiss="modal"
                                                                                    aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <div class="row">
                                                                                <section id="basic" style="width: 100%;">
                                                                                    <div class="card">
                                                                                        <div class="card-content collapse show">
                                                                                            <div class="card-body">
                                                                                                <form class="form-horizontal" action="#">
                                                                                                    <div class="form-group">
                                                                                                        <div class="row">
                                                                                                            <div class="col-lg-12">
                                                                                                                <textarea class="summernote"  name="ourfleetmore" id="ourfleetmore">{{$data->ourfleetmore}}</textarea>

                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </form>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                </section>
                                                                            </div>
                                                                            {{--<button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Update</button>--}}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <button type="button" class="btn btn-icon btn-outline-primary"
                                                                    data-toggle="modal"
                                                                    data-target="#editsummary{{$data->id}}">
                                                                Edit
                                                            </button>
                                                            <!-- Modal -->
                                                            <div class="modal fade text-left" id="editsummary{{$data->id}}" tabindex="-1"
                                                                 role="dialog" aria-labelledby="myModalLabel1"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title" id="myModalLabel1"> Activity Summary</h4>
                                                                            <button type="button" class="close" data-dismiss="modal"
                                                                                    aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <form class="form-horizontal form-simple" method="POST" action="{{ url('EditOurFleetRow') }}" enctype="multipart/form-data">
                                                                                {{ csrf_field() }}
                                                                                <div class="row">
                                                                                    <div class="col-md-6">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Our Fleet  Caption</label>
                                                                                            <input type="text" id="projectinput1" class="form-control" value="{{$data->ourfleetcaption}}"
                                                                                                   name="ourfleetcaption">
                                                                                            <input type="text" id="projectinput1" class="form-control" value="{{$data->id}}"
                                                                                                   name="id" hidden>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Our Fleet  Image</label>
                                                                                            <input type="file" id="projectinput1" class="form-control"
                                                                                                   name="ourfleetimage">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <section id="basic" style="width: 100%;">
                                                                                        <div class="card">
                                                                                            <div class="card-content collapse show">
                                                                                                <div class="card-body">
                                                                                                    <div class="form-group">
                                                                                                        <div class="row">
                                                                                                            <div class="col-lg-12">
                                                                                                                <textarea class="summernote"  name="ourfleetmore" id="ourfleetmore">{{$data->ourfleetmore}}</textarea>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>

                                                                                    </section>
                                                                                </div>
                                                                                <div class="col-lg-12">
                                                                                    <img src="OurFleet/{{$data->ourfleetimage}}" style="width: 100%;padding-bottom: 10px">
                                                                                </div>
                                                                                <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Update</button>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td><a href="{{ route('backend.DeleteAttraction',['id'=> $data->id])}}" class="btn btn-icon btn-outline-primary">Delete</a></td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section id="setting">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        {{--<h4 class="card-title">Destination list</h4>--}}
                                        @if (session('success'))
                                            <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                                {{ session('success') }}
                                            </div>
                                        @endif
                                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <table class="table table-striped table-bordered dataex-html5-export table-responsive">
                                                <thead>
                                                <tr>
                                                    <th>Fleet Gallery Image</th>
                                                    <th>Date Created</th>
                                                    <th>Delete</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($listgallery as $datas)
                                                    <tr>
                                                        <td><img src="OurFleet/{{$datas->fleetgallery}}" style="width: 100%;padding-bottom: 10px"></td>
                                                        <td>{{$datas->created_at}}</td>
                                                        <td><a href="{{ route('backend.DeleteOurFleetRow',['id'=> $data->id])}}" class="btn btn-icon btn-outline-primary">Delete</a></td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <!-- Summernote Click to edit end -->
                    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/vendors/js/editors/summernote/summernote.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/scripts/editors/editor-summernote.min.js" type="text/javascript"></script>
                </div>
            </div>
        </div>
    </div>

@endsection
