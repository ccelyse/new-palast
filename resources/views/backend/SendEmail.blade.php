@extends('backend.layout.master')

@section('title', 'RHA')

@section('content')
    <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <style>
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary:hover{
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
    </style>
    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body">
                <div class="content-body">
                    <!-- Basic Summernote start -->
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="form-horizontal form-simple" method="POST" action="{{url('SendEmail_')}}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Email Subject</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{ old('emailsubject') }}" placeholder="Email Subject"
                                                                   name="emailsubject" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">

                                                                <div class="form-group">
                                                                    <label for="projectinput1">To</label>
                                                                    <select class="select2 form-control" name="emailto">
                                                                        @foreach($listnumber as $data)
                                                                            <option value="{{$data->email}}">{{$data->companyname}}</option>
                                                                        @endforeach
                                                                    </select>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <section id="basic" style="width: 100%;">
                                                        <div class="card">
                                                            <div class="card-content collapse show">
                                                                <div class="card-body">
                                                                    <form class="form-horizontal" action="#">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-lg-12">
                                                                                    <textarea class="summernote"  name="emailcontent" required>{{ old('emailcontent') }}</textarea>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                </div>
                                                <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Save</button>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <section id="summernote-edit-save" hidden>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Summernote Click to edit</h4>
                                        <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body">
                                            <form class="form-horizontal" action="#">
                                                <div class="form-group">
                                                    <button id="edit" class="btn btn-primary" type="button"><i class="la la-pencil"></i> Edit</button>
                                                    <button id="save" class="btn btn-success" type="button"><i class="la la-save"></i> Save</button>
                                                </div>
                                                <div class="form-group">
                                                    <div class="summernote-edit">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/vendors/js/editors/summernote/summernote.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/scripts/editors/editor-summernote.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/scripts/forms/select/form-select2.min.js" type="text/javascript"></script>
                </div>
            </div>
        </div>
    </div>

@endsection
