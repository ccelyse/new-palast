@extends('backend.layout.master')

@section('title', 'Palast')

@section('content')
    <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <style>
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #ff9027 !important;
            border-color: #ff9027 !important;
        }
        .btn-primary:hover{
            background-color: #ff9027 !important;
            border-color:#ff9027 !important;
        }
    </style>
    {{--<script--}}
            {{--src="https://code.jquery.com/jquery-3.3.1.min.js"--}}
            {{--integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="--}}
            {{--crossorigin="anonymous"></script>--}}

    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body">
                <div class="content-body">
                    <!-- Basic Summernote start -->
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    <h4 class="card-title" id="basic-layout-form">Attraction Info</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('AddAttractions_') }}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="form-body">
                                                <h4 class="form-section"><i class="fas fa-map-marker-alt"></i>Attraction Location</h4>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Province</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{ old('name') }}" placeholder="Province"
                                                                   name="attraction_province" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput2">Attraction name</label>
                                                            <input type="text" id="projectinput2" class="form-control" value="{{ old('name') }}" placeholder="Attractionname"
                                                                   name="attraction_name" required>
                                                        </div>
                                                    </div>
                                                </div>

                                                <h4 class="form-section"><i class="fas fa-image"></i>Attraction Cover Picture</h4>
                                                <div class="row">
                                                    <div class="card">
                                                        <div class="card-header">
                                                            {{--<label class="card-title" for="exampleInputFile">File input</label>--}}
                                                        </div>
                                                        <div class="card-block">
                                                            <div class="card-body">
                                                                <fieldset class="form-group">
                                                                    <input type="file" class="form-control-file" id="exampleInputFile" name="fileToUpload" required>
                                                                </fieldset>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <h4 class="form-section"><i class="fas fa-clipboard-check"></i> Attraction In details</h4>
                                                <div class="row">
                                                    <section id="basic" style="width: 100%;">
                                                        <div class="card">
                                                            <div class="card-content collapse show">
                                                                <div class="card-body">
                                                                    <form class="form-horizontal" action="#">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-lg-12">
                                                                                    <textarea class="summernote"  name="attraction_indetails" id="attraction_indetails"  required>{{ old('name') }}</textarea>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </section>
                                                </div>
                                                <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Save</button>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <section id="setting">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Destination list</h4>
                                        @if (session('success'))
                                            <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                                {{ session('success') }}
                                            </div>
                                        @endif
                                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <table class="table table-striped table-bordered dataex-html5-export table-responsive">
                                                <thead>
                                                <tr>
                                                    <th>Attraction Province</th>
                                                    <th>Attraction name</th>
                                                    <th>Attraction Cover Picture</th>
                                                    <th>Attraction In details</th>
                                                    <th>Edit</th>
                                                    <th>Delete</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($listattractions as $data)
                                                    <tr>
                                                        <td>{{$data->attraction_province}}</td>
                                                        <td>{{$data->attraction_name}}</td>
                                                        <td><img src="attractions/{{$data->attraction_image}}" style="width: 100%;padding-bottom: 10px"></td>
                                                        <td>
                                                            <button type="button" class="btn btn-icon btn-outline-primary"
                                                                    data-toggle="modal"
                                                                    data-target="#summary{{$data->id}}">
                                                                Attraction Details
                                                            </button>
                                                            <!-- Modal -->
                                                            <div class="modal fade text-left" id="summary{{$data->id}}" tabindex="-1"
                                                                 role="dialog" aria-labelledby="myModalLabel1"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title" id="myModalLabel1"> Activity Summary</h4>
                                                                            <button type="button" class="close" data-dismiss="modal"
                                                                                    aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <div class="row">
                                                                                <section id="basic" style="width: 100%;">
                                                                                    <div class="card">
                                                                                        <div class="card-content collapse show">
                                                                                            <div class="card-body">
                                                                                                <form class="form-horizontal" action="#">
                                                                                                    <div class="form-group">
                                                                                                        <div class="row">
                                                                                                            <div class="col-lg-12">
                                                                                                                <textarea class="summernote"  name="attraction_indetails" id="attraction_indetails">{{$data->attraction_indetails}}</textarea>

                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </form>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                </section>
                                                                            </div>
                                                                            {{--<button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Update</button>--}}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <button type="button" class="btn btn-icon btn-outline-primary"
                                                                    data-toggle="modal"
                                                                    data-target="#editsummary{{$data->id}}">
                                                                Edit
                                                            </button>
                                                            <!-- Modal -->
                                                            <div class="modal fade text-left" id="editsummary{{$data->id}}" tabindex="-1"
                                                                 role="dialog" aria-labelledby="myModalLabel1"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title" id="myModalLabel1"> Activity Summary</h4>
                                                                            <button type="button" class="close" data-dismiss="modal"
                                                                                    aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <form class="form-horizontal form-simple" method="POST" action="{{ url('EditAttractions') }}" enctype="multipart/form-data">
                                                                                {{ csrf_field() }}
                                                                                <div class="form-body">
                                                                                    <h4 class="form-section"><i class="fas fa-map-marker-alt"></i>Attraction Location</h4>
                                                                                    <div class="row">
                                                                                        <div class="col-md-6">
                                                                                            <div class="form-group">
                                                                                                <label for="projectinput1">Province</label>
                                                                                                <input type="text" id="projectinput1" class="form-control" value="{{$data->attraction_province}}"
                                                                                                       name="attraction_province">

                                                                                                <input type="text" id="projectinput1" class="form-control" value="{{$data->id}}"
                                                                                                       name="id" hidden>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-6">
                                                                                            <div class="form-group">
                                                                                                <label for="projectinput2">Attraction name</label>
                                                                                                <input type="text" id="projectinput2" class="form-control" value="{{$data->attraction_name}}"
                                                                                                       name="attraction_name">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <h4 class="form-section"><i class="fas fa-image"></i>Attraction Cover Picture</h4>
                                                                                    <div class="row">
                                                                                        <div class="card">
                                                                                            <div class="card-header">
                                                                                                {{--<label class="card-title" for="exampleInputFile">File input</label>--}}
                                                                                            </div>
                                                                                            <div class="card-block">
                                                                                                <div class="card-body">
                                                                                                    <fieldset class="form-group">
                                                                                                        <input type="file" class="form-control-file" id="exampleInputFile" name="fileToUpload">
                                                                                                    </fieldset>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <h4 class="form-section"><i class="fas fa-clipboard-check"></i> Attraction In details</h4>
                                                                                    <div class="row">
                                                                                        <section id="basic" style="width: 100%;">
                                                                                            <div class="card">
                                                                                                <div class="card-content collapse show">
                                                                                                    <div class="card-body">
                                                                                                        <form class="form-horizontal" action="#">
                                                                                                            <div class="form-group">
                                                                                                                <div class="row">
                                                                                                                    <div class="col-lg-12">
                                                                                                                        <textarea class="summernote"  name="attraction_indetails" id="attraction_indetails" >{{$data->attraction_indetails}}</textarea>

                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </form>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>

                                                                                        </section>
                                                                                    </div>
                                                                                    <div class="col-lg-12">
                                                                                        <img src="attractions/{{$data->attraction_image}}" style="width: 100%;padding-bottom: 10px">
                                                                                    </div>
                                                                                    <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Update</button>
                                                                                </div>
                                                                            </form>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td><a href="{{ route('backend.DeleteAttraction',['id'=> $data->id])}}" class="btn btn-icon btn-outline-primary">Delete</a></td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section id="summernote-edit-save" >
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Summernote Click to edit</h4>
                                        <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body">
                                            <form class="form-horizontal" action="#">
                                                <div class="form-group">
                                                    <button id="edit" class="btn btn-primary" type="button"><i class="la la-pencil"></i> Edit</button>
                                                    <button id="save" class="btn btn-success" type="button"><i class="la la-save"></i> Save</button>
                                                </div>
                                                <div class="form-group">
                                                    <div class="summernote-edit">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <!-- Summernote Click to edit end -->
                    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/vendors/js/editors/summernote/summernote.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/scripts/editors/editor-summernote.min.js" type="text/javascript"></script>
                </div>
            </div>
        </div>
    </div>

@endsection
