@extends('backend.layout.master')

@section('title', 'Palast')

@section('content')
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">
    <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <style>
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary:hover{
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #b4753c  !important;
            border-color:#b4753c  !important;
        }
        .btn-primary:hover{
            background-color: #b4753c !important;
            border-color:#b4753c !important;
        }
        .btn-dark1 {
            color: #000 !important;
            background-color: transparent;
            border-color: #b4753c !important;
        }
        .btn-dark1:hover {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        .btn-dark {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        .btn-dark:hover {
            color: #000 !important;
            background-color: transparent;
            border-color: #b4753c !important;
        }
    </style>
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script type="text/javascript">
        $(document).ready(function()
        {
            $('.multi-field-wrapper').each(function() {
                var $wrapper = $('.multi-fields', this);
                $(".add-field", $(this)).click(function(e) {
                    $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
                });
                $('.multi-field .remove-field', $wrapper).click(function() {
                    if ($('.multi-field', $wrapper).length > 1)
                        $(this).parent('.multi-field').remove();
                });
            });
        });
        $(document).ready(function()
        {
            $('.multi-field-wrapper2').each(function() {
                var $wrapper = $('.multi-fields2', this);
                $(".add-field2", $(this)).click(function(e) {
                    $('.multi-field2:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
                });
                $('.multi-field2 .remove-field2', $wrapper).click(function() {
                    if ($('.multi-field2', $wrapper).length > 1)
                        $(this).parent('.multi-field2').remove();
                });
            });
        });
    </script>
    <link rel="stylesheet" type="text/css" href="backend/app-assets/css/pages/timeline.min.css">
    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body">
                <div class="content-body">
                    <!-- Basic Summernote start -->
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    <h4 class="card-title" id="basic-layout-form">Add Destination</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('UploadDestination') }}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="projectinput1">Activity Title</label>
                                                        <input type="text" id="projectinput1" class="form-control" value="{{ old('activitytitle') }}" placeholder="Activity Title"
                                                               name="activitytitle" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="projectinput1">Activity Cover Image</label>
                                                        <input type="file" id="projectinput1" class="form-control"
                                                               name="activityimage" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="projectinput1">Activity Summary</label>
                                                        <textarea id="summernote" class="form-control"  name="activity_summary">{{ old('activitysummary') }}</textarea>
{{--                                                        <textarea  name="activity_summary" class="form-control" id="activitysummary" rows="10"></textarea>--}}
                                                    </div>
                                                </div>
                                            </div>

{{--                                            <section id="" style="padding-bottom: 10px">--}}
{{--                                                <h3 class="page-title text-center" style="font-weight: bold;">Activity in details</h3>--}}
{{--                                                <div class="multi-field-wrapper">--}}
{{--                                                    <div class="multi-fields">--}}
{{--                                                        <div class="multi-field">--}}
{{--                                                            <div class="timeline-card card border-grey border-lighten-2">--}}
{{--                                                                <div class="card-content">--}}
{{--                                                                    <div class="row">--}}
{{--                                                                        <div class="col-md-6">--}}
{{--                                                                            <div class="form-group">--}}
{{--                                                                                <label for="projectinput1">Number of days</label>--}}
{{--                                                                                <input type="text" id="projectinput1" class="form-control" value="{{ old('activityday') }}" placeholder="Number of days" name="activityday[]" required>--}}
{{--                                                                            </div>--}}
{{--                                                                        </div>--}}
{{--                                                                        <div class="col-md-6">--}}
{{--                                                                            <div class="form-group">--}}
{{--                                                                                <label for="projectinput1">Activity Location</label>--}}
{{--                                                                                <input type="text" id="projectinput1" class="form-control" value="{{ old('activitylocation') }}" placeholder="Activity Location" name="activitylocation[]" required>--}}
{{--                                                                            </div>--}}
{{--                                                                        </div>--}}
{{--                                                                    </div>--}}
{{--                                                                    <div class="row">--}}
{{--                                                                        <div class="col-md-12">--}}
{{--                                                                            <div class="form-group">--}}
{{--                                                                                <label for="projectinput1">Activity details</label>--}}
{{--                                                                                <textarea id="summernote2" class="form-control" name="activitydetails[]">{{ old('activitydetails') }}</textarea>--}}
{{--                                                                                --}}{{----}}
{{--                                                                                <textarea name="activity_summary" class="form-control" id="activitysummary" rows="10"></textarea>--}}
{{--                                                                            </div>--}}
{{--                                                                        </div>--}}
{{--                                                                    </div>--}}

{{--                                                                </div>--}}
{{--                                                            </div>--}}

{{--                                                        </div>--}}
{{--                                                    </div>--}}

{{--                                                </div>--}}
{{--                                            </section>--}}
                                            <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Save</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <section id="setting">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Activity list</h4>
                                        @if (session('success'))
                                            <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                                {{ session('success') }}
                                            </div>
                                        @endif
                                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <table class="table table-striped table-bordered dataex-html5-export">
                                                <thead>
                                                <tr>
                                                    <th>Activity Title</th>
                                                    <th>Activity Cover Picture</th>
{{--                                                    <th>Activity Summary</th>--}}
                                                    <th>Edit Activity Summary</th>
                                                    <th>Delete</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($listactivity as $data)
                                                    <tr>
                                                        <td>{{$data->activitytitle}}</td>
                                                        <td><img src="ActivityCoverImage/{{$data->activityimage}}" style="width: 200px;padding-bottom: 10px"></td>

{{--                                                        <td>--}}
{{--                                                            <button type="button" class="btn btn-icon btn-outline-primary"--}}
{{--                                                                    data-toggle="modal"--}}
{{--                                                                    data-target="#editsummary{{$data->id}}">--}}
{{--                                                                Edit Activity Summary--}}
{{--                                                            </button>--}}
{{--                                                            <!-- Modal -->--}}
{{--                                                            <div class="modal fade text-left" id="editsummary{{$data->id}}" tabindex="-1"--}}
{{--                                                                 role="dialog" aria-labelledby="myModalLabel1"--}}
{{--                                                                 aria-hidden="true">--}}
{{--                                                                <div class="modal-dialog" role="document">--}}
{{--                                                                    <div class="modal-content">--}}
{{--                                                                        <div class="modal-header">--}}
{{--                                                                            <h4 class="modal-title" id="myModalLabel1"> Activity Summary</h4>--}}
{{--                                                                            <button type="button" class="close" data-dismiss="modal"--}}
{{--                                                                                    aria-label="Close">--}}
{{--                                                                                <span aria-hidden="true">&times;</span>--}}
{{--                                                                            </button>--}}
{{--                                                                        </div>--}}
{{--                                                                        <div class="modal-body">--}}
{{--                                                                            <form class="form-horizontal form-simple" method="POST" action="{{ url('EditActivitySummary') }}" enctype="multipart/form-data">--}}
{{--                                                                                {{ csrf_field() }}--}}
{{--                                                                            <div class="row">--}}
{{--                                                                                <div class="col-md-12">--}}
{{--                                                                                    <div class="form-group">--}}
{{--                                                                                        <label for="projectinput1">Activity Title</label>--}}
{{--                                                                                        <input type="text" id="projectinput1" class="form-control" value="{{$data->activitytitle}}" placeholder="Activity Title"--}}
{{--                                                                                               name="activitytitle">--}}

{{--                                                                                        <input type="text" id="projectinput1" class="form-control" value="{{$data->id}}"--}}
{{--                                                                                               name="id" hidden>--}}
{{--                                                                                    </div>--}}
{{--                                                                                </div>--}}
{{--                                                                                <div class="col-md-12">--}}
{{--                                                                                    <div class="form-group">--}}
{{--                                                                                        <label for="projectinput1">Activity Cover Image</label>--}}
{{--                                                                                        <input type="file" id="projectinput1" class="form-control"--}}
{{--                                                                                               name="activityimage">--}}
{{--                                                                                    </div>--}}
{{--                                                                                </div>--}}

{{--                                                                                <div class="col-md-12">--}}
{{--                                                                                    <div class="form-group">--}}
{{--                                                                                        <label for="projectinput1">Activity Summary</label>--}}
{{--                                                                                        <textarea id="summernote4" class="form-control"  name="activity_summary">{{$data->activitysummary}}</textarea>--}}
{{--                                                                                        <textarea  name="activity_summary" class="form-control" id="activitysummary" rows="10">{{$data->activitysummary}}</textarea>--}}
{{--                                                                                    </div>--}}
{{--                                                                                </div>--}}
{{--                                                                                <div class="col-md-12">--}}
{{--                                                                                    <img src="ActivityCoverImage/{{$data->activityimage}}" style="width: 100%;padding-bottom: 10px">--}}
{{--                                                                                </div>--}}
{{--                                                                            </div>--}}
{{--                                                                                <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Update</button>--}}
{{--                                                                            </form>--}}

{{--                                                                        </div>--}}
{{--                                                                    </div>--}}
{{--                                                                </div>--}}
{{--                                                            </div>--}}
{{--                                                        </td>--}}
                                                        <td><a href="{{ route('backend.EditDestinations',['id'=> $data->id])}}" class="btn btn-icon btn-outline-primary">Edit Summary</a></td>
                                                        <td><a href="{{ route('backend.DeleteActivitySummary',['id'=> $data->id])}}" class="btn btn-icon btn-outline-primary">Delete</a></td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <script src="../js/summernote.js" type="application/javascript"></script>
                    <script>
                        $(document).ready(function() {
                            $('#summernote').summernote();
                            $('#summernote2').summernote();
                            $('#summernote3').summernote();
                            $('#summernote4').summernote();
                        });
                    </script>
                    <!-- Summernote Click to edit end -->
                    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/scripts/pages/timeline.min.js"></script>
                </div>
            </div>
        </div>
    </div>

@endsection
