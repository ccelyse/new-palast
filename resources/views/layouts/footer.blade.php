<style>
    .eltd-page-footer .widget.widget_nav_menu ul li a {
        font-size: 12px !important;
    }
</style>
<footer class="eltd-page-footer">
        <div class="eltd-footer-bottom-holder eltd-footer-bottom-skin-dark">
            <div class="eltd-footer-bottom-inner eltd-full-width">
                <div class="eltd-grid-row">
                    <div class="eltd-grid-col-6">
                        <div id="media_image-3" class="widget eltd-footer-bottom-column-1 widget_media_image"><img width="190" height="27" src="frontend/assets/images/logo.png" class="image wp-image-2685  attachment-full size-full" alt="a" style="max-width: 100%; height: auto;" /></div>
                        <div id="media_image-4" class="widget eltd-footer-bottom-column-1 widget_media_image"><img width="190" height="27" src="frontend/assets/images/logo.png" class="image wp-image-2679  attachment-full size-full" alt="a" style="max-width: 100%; height: auto;" /></div>
                    </div>
                    <div class="eltd-grid-col-6">
                        <div id="nav_menu-2" class="widget eltd-footer-bottom-column-2 widget_nav_menu">
                            <div class="menu-menu-footer-container">
                                <ul id="menu-menu-footer" class="menu">
                                    <li id="menu-item-239" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-239"><a href="{{url('/')}}">Home</a></li>
                                    <li id="menu-item-240" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-240"><a href="{{url('Destinations')}}">Destinations</a></li>
                                    <li id="menu-item-243" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-243"><a href="{{url('SafariExperiences')}}">Safari Experience</a></li>
                                    <li id="menu-item-243" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-243"><a href="{{url('ContactUs')}}">Contact us</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <script type='text/javascript'>
        /* <![CDATA[ */
        var wpcf7 = {"apiSettings":{"root":"http:\/\/bonvoyage.elated-themes.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"Please verify that you are not a robot."}},"cached":"1"};
        /* ]]> */
    </script>
    <script type="text/javascript" src="frontend/wp-content/cache/minify/e0bea.js"></script>

    <script type='text/javascript'>
        jQuery(document).ready(function(jQuery){jQuery.datepicker.setDefaults({"closeText":"Close","currentText":"Today","monthNames":["January","February","March","April","May","June","July","August","September","October","November","December"],"monthNamesShort":["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"nextText":"Next","prevText":"Previous","dayNames":["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],"dayNamesShort":["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],"dayNamesMin":["S","M","T","W","T","F","S"],"dateFormat":"MM d, yy","firstDay":1,"isRTL":false});});
    </script>

    <script type="text/javascript" src="frontend/wp-content/cache/minify/0ded2.js"></script>

    <script type='text/javascript'>
        /* <![CDATA[ */
        var eltdGlobalVars = {"vars":{"eltdAddForAdminBar":0,"eltdElementAppearAmount":-100,"eltdAjaxUrl":"http:\/\/bonvoyage.elated-themes.com\/wp-admin\/admin-ajax.php","eltdStickyHeaderHeight":70,"eltdStickyHeaderTransparencyHeight":70,"eltdTopBarHeight":40,"eltdLogoAreaHeight":0,"eltdMenuAreaHeight":130,"eltdMobileHeaderHeight":70}};
        var eltdPerPageVars = {"vars":{"eltdStickyScrollAmount":0,"eltdHeaderTransparencyHeight":0,"eltdHeaderVerticalWidth":0}};
        /* ]]> */
    </script>

    <script type="text/javascript" src="frontend/wp-content/cache/minify/f0160.js"></script>


    <script type="text/javascript" src="frontend/wp-content/cache/minify/fe83b.js"></script>

    <script type='text/javascript'>
        /* <![CDATA[ */
        var woocommerce_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%"};
        /* ]]> */
    </script>
    <script type="text/javascript" src="frontend/wp-content/cache/minify/63a69.js"></script>

    <script type='text/javascript'>
        /* <![CDATA[ */
        var wc_cart_fragments_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_2808991c8be127e2dfee5b31dbbf272a","fragment_name":"wc_fragments_2808991c8be127e2dfee5b31dbbf272a"};
        /* ]]> */
    </script>
    <script type="text/javascript" src="frontend/wp-content/cache/minify/e8bf2.js"></script>
    </body>
    </html>