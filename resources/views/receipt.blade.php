<!DOCTYPE html>
<html>
<head>
    <title>PDF Receipt</title>
</head>
<body style="font-size: 8px; font-family: Arial">

<div align="center">

    <div style="width: 100%; text-align: center; font-size: 20px">
        <p>
            <b>Palast Tours Receipt</b>
        </p>
    </div>

    <table style = "width: 100%; font-size: 14px">
        <tr>
            <td style ="width: 20%;">
                <img src="{{URL::asset('backend/app-assets/images/logo.png')}}" />
                {{--<img src="/Users/pa6/Documents/Sites/raisins/mergimstest/public/assets/images/apple-touch-icon.png" width="30px" height="40px" />--}}
            </td>
            <td style="width: 60%">

            </td>
            <td style="width: 20%; height: 50px;">
                Date: <?php echo Date("Y-m-d") ?>
                @foreach($getnames as $data)
                    <p>Customer : {{$data->enquire_first_name or ''}} {{$data->last_name or ''}}</p>
                    <p>Phone: {{$data->enquire_telephone or ''}}</p>
                    <p>Email: {{$data->enquire_email or ''}}</p>
                    <p> Invoice number: {{$data->enquire_code}}</p>
                    <p> Country: {{$data->enquire_country}}</p>
                @endforeach
            </td>
        </tr>
        <tr>
            <td>
                <p><b>Palast Tours Ltd </b></p>
                <p>Rwanda-Kigali-Kicukiro</p>
                <p>Tel: +250784674639</p>
                <p>E-mail: info@palastours.rw</p>
            </td>
        </tr>

    </table>
    </br>
    <div style="width: 90%; padding: 0px; text-align: left; margin-bottom: 50px; font-size: 15px">

    </div>
    <table  style="border: 1px solid #ff9027; width: 100%; border-radius: 5px; font-size: 15px">
        <tr style="background-color: #ff9027; color: #fff">
            <td>Destination name</td>
            <td>Quantity</td>
            <td>Price</td>
        </tr>
        <tr style="padding: 10px">
            <td>
                <?php echo $getdestinationinfo?>
            </td>
            <td>
                1
            </td>
            <td>
                <?php echo "$$vpc_Amount"?>
            </td>

        </tr>
    </table>



    <p><strong>This is official receipt of Palast Tours Ltd</strong></br>


</div>
<div style="margin-top: 50px; text-align: center; width: 100%">
    <table border="0" width="600px" cellspacing="0" cellpadding="0" style="border-collapse: collapse;">
        <tr>
            <td>
                {{--<img src="data:image/png;base64,{{DNS1D::getBarcodePNG($payment->code, 'C128')}}" alt="receipt " width="460px"/>--}}
            </td>

        </tr>
    </table>
</div>
</body>
</html>